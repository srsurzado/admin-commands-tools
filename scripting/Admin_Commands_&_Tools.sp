#include <sourcemod>
#include <sdktools>
#include <cstrike>

#define Url "http://steamcommunity.com/profiles/76561198312713613"
#define Author "Marko (M4rk0)"
#define DATA "2.5.1"

public Plugin:myinfo = {
    name = "Admin Commands & Tools",
	author = Author,
	description = "",
	version = DATA,
	url = Url
};

public OnPluginStart()
{
    //Admin Commands
	RegAdminCmd("sm_admintools", Cmd_AdminTools, ADMFLAG_GENERIC);
	RegAdminCmd("sm_admintool", Cmd_AdminTools, ADMFLAG_GENERIC);
	RegAdminCmd("sm_admincommands", Cmd_AdminTools, ADMFLAG_GENERIC);
	RegAdminCmd("sm_admincommand", Cmd_AdminTools, ADMFLAG_GENERIC);
	RegAdminCmd("sm_admincmds", Cmd_AdminTools, ADMFLAG_GENERIC);
	RegAdminCmd("sm_admincmd", Cmd_AdminTools, ADMFLAG_GENERIC);
	RegAdminCmd("sm_restartgame", Cmd_RestartGame, ADMFLAG_GENERIC, "Restarting round");
	RegAdminCmd("sm_restart", Cmd_RestartGame, ADMFLAG_GENERIC, "Restarting, round");
	RegAdminCmd("sm_pausematch", Cmd_PauseMatch, ADMFLAG_GENERIC, "Pausing match");
	RegAdminCmd("sm_pause", Cmd_PauseMatch, ADMFLAG_GENERIC, "Pausing match");
	RegAdminCmd("sm_unpausematch", Cmd_UnPauseMatch, ADMFLAG_GENERIC, "Unpausing match");
	RegAdminCmd("sm_unpause", Cmd_UnPauseMatch, ADMFLAG_GENERIC, "Unpausing match");
	
	
	//Weapon Commands
	RegAdminCmd("sm_secondaryweapons", Cmd_SecondaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_secondaryweapon", Cmd_SecondaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_secondary", Cmd_SecondaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_secondarys", Cmd_SecondaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_pistols", Cmd_SecondaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_pistol", Cmd_SecondaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_primaryweapons", Cmd_PrimaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_primaryweapon", Cmd_PrimaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_primary", Cmd_PrimaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_primarys", Cmd_PrimaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_rifles", Cmd_PrimaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_rifle", Cmd_PrimaryWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_heavyweapons", Cmd_HeavyWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_heavy", Cmd_HeavyWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_smg", Cmd_SMGWeapons, ADMFLAG_GENERIC);
	RegAdminCmd("sm_smgs", Cmd_SMGWeapons, ADMFLAG_GENERIC);
	
	//Admin Info Commands
	RegConsoleCmd("sm_info", Cmd_AdminCommandsInfo);
	RegConsoleCmd("sm_admininfo", Cmd_AdminCommandsInfo);
	RegConsoleCmd("sm_admininformation", Cmd_AdminCommandsInfo);
	RegConsoleCmd("sm_information", Cmd_AdminCommandsInfo);
	RegConsoleCmd("sm_author", Cmd_Author);
	RegConsoleCmd("sm_creator", Cmd_Author);
	RegConsoleCmd("sm_plugininfo", Cmd_PluginInfo);
	RegConsoleCmd("sm_plugininformation", Cmd_PluginInfo);
}

public Action Cmd_AdminTools(int client, int args)
{
    new Handle:menuhandle1 = CreateMenu(Cmd_AdminTools2);
	SetMenuTitle(menuhandle1, "Admin Commands & Tools Version: %s.\nPlugin Created By %s.\nCheck Author Info for more information", DATA, Author);
	
	//Server Commands
	AddMenuItem(menuhandle1, "restart", "Restart Game");
	AddMenuItem(menuhandle1, "pause", "Pause Game");
	AddMenuItem(menuhandle1, "unpause", "Unpause Game");

    //Creator Info
    AddMenuItem(menuhandle1, "author", "Author Info");

    //Plugin Info
    AddMenuItem(menuhandle1, "plugininfo", "Plugin Info");	
	
	//Secondary Weapons
	AddMenuItem(menuhandle1, "usp_silencer", "USP-S");
	AddMenuItem(menuhandle1, "glock", "Glock-18");
	AddMenuItem(menuhandle1, "elite", "Dual Berretas");
	AddMenuItem(menuhandle1, "p250", "P250");
	AddMenuItem(menuhandle1, "fiveseven", "Five SeveN");
	AddMenuItem(menuhandle1, "tec9", "Tec-9")
	AddMenuItem(menuhandle1, "cz75a", "CZ75-Auto");
	AddMenuItem(menuhandle1, "deagle", "Desert Eagle");
	
	//Primary Weapons
	AddMenuItem(menuhandle1, "galilar", "Galil AR");
	AddMenuItem(menuhandle1, "famas", "FAMAS");
	AddMenuItem(menuhandle1, "ak47", "AK-47");
	AddMenuItem(menuhandle1, "m4a1_silencer", "M4A1-S");
	AddMenuItem(menuhandle1, "m4a1", "M4A4");
	AddMenuItem(menuhandle1, "ssg08", "SSG 08");
	AddMenuItem(menuhandle1, "sg553", "SG 553");
	AddMenuItem(menuhandle1, "aug", "AUG");
	AddMenuItem(menuhandle1, "awp", "AWP");
	AddMenuItem(menuhandle1, "g3sg1", "G3SG1");
	AddMenuItem(menuhandle1, "scar20", "SCAR-20");
	
	//Heavy Weapons
	AddMenuItem(menuhandle1, "nova", "Nova");
	AddMenuItem(menuhandle1, "xm1014", "XM1014");
	AddMenuItem(menuhandle1, "mag7", "MAG-7");
	AddMenuItem(menuhandle1, "sawedoff", "Sawed-Off");
	AddMenuItem(menuhandle1, "m249", "M249");
	AddMenuItem(menuhandle1, "negev", "Negev");
	
	//SMGs
	AddMenuItem(menuhandle1, "mac10", "MAC-10");
	AddMenuItem(menuhandle1, "mp9", "MP9");
	AddMenuItem(menuhandle1, "mp7", "MP7");
	AddMenuItem(menuhandle1, "ump45", "UMP-45");
	AddMenuItem(menuhandle1, "p90", "P90");
	AddMenuItem(menuhandle1, "bizon", "PP-Bizon");
	
	SetMenuPagination(menuhandle1, 7);
	SetMenuExitButton(menuhandle1, true);
	DisplayMenu(menuhandle1, client, 0);
	
	return Plugin_Handled;
	
}

public Cmd_AdminTools2(Handle:menuhandle1, MenuAction:action, client, ItemNum)
{
    if(action == MenuAction_Select )
	{
	    switch(ItemNum)
		{
		    case 0:
			{
			    ServerCommand("mp_restartgame 5");
				PrintToChatAll("\x04[SM]\x01 Restarting Game!");
				PrintToChatAll("\x04[SM]\x01 Restarting Game!");
				PrintToChatAll("\x04[SM]\x01 Restarting Game!");
			}
			case 1:
			{
			    ServerCommand("mp_pause_match 1");
				PrintToChatAll("\x04[SM]\x01 Pausing Match!")
				PrintToChatAll("\x04[SM]\x01 Pausing Match!")
				PrintToChatAll("\x04[SM]\x01 Pausing Match!")
			}
			case 2:
			{
			    ServerCommand("mp_unpause_match 1");
				PrintToChatAll("\x04[SM]\x01 Unpausing Match");
				PrintToChatAll("\x04[SM]\x01 Resuming Match");
				PrintToChatAll("\x04[SM]\x01 Unpausing Match");
			}
			case 3:
			{
			    PrintToChat(client, "\x04[SM]\x01 The Creator of this plugin is \x0E%s\x01. Check Console for more info.", Author);
			    PrintToConsole(client, "---------------------------------------------------------------------------");
				PrintToConsole(client, "Name = %s", Author);
				PrintToConsole(client, "Real Name = Markus");
				PrintToConsole(client, "Steam Url = %s", Url);
				PrintToConsole(client, "Currently Community = CSROG");
				PrintToConsole(client, "Community Steam Group = http://steamcommunity.com/groups/csrogcommunity");
				PrintToConsole(client, "Community Forum = http://csrog.com");
				PrintToConsole(client, "%s Is also making private plugins for a small bit of money.", Author);
				PrintToConsole(client, "---------------------------------------------------------------------------");
			}
			case 4:
			{
			    PrintHintText(client, " The Creator of this plugin is \x0E%s\x01. More Plugin Info Check Console.", Author);
				PrintToChat(client, "\x04[SM]\x01 The Creator of this plugin is \x0E%s\x01. More Plugin Info Check Console.", Author);
				PrintToConsole(client, "---------------------------------------------------------------------------");
				PrintToConsole(client, "Author = %s", Author);
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Commands for this plugin working for all admins:");
				PrintToConsole(client, "!admintools");
				PrintToConsole(client, "!admintool");
				PrintToConsole(client, "!admincommands");
				PrintToConsole(client, "!admincommand");
				PrintToConsole(client, "!admincmds");
				PrintToConsole(client, "!admincmd");
				PrintToConsole(client, "!restartgame");
				PrintToConsole(client, "!restart");
				PrintToConsole(client, "!pausematch");
				PrintToConsole(client, "!pause");
				PrintToConsole(client, "!unpausematch");
				PrintToConsole(client, "!unpause");
				PrintToConsole(client, "!secondaryweapons");
				PrintToConsole(client, "!secondaryweapon");
				PrintToConsole(client, "!secondary");
				PrintToConsole(client, "!secondarys");
				PrintToConsole(client, "!pistols");
				PrintToConsole(client, "!pistol");
				PrintToConsole(client, "!primaryweapons");
				PrintToConsole(client, "!primaryweapon");
				PrintToConsole(client, "!primary");
				PrintToConsole(client, "!primarys");
				PrintToConsole(client, "!rifles");
				PrintToConsole(client, "!rifle");
				PrintToConsole(client, "!heavyweapons");
				PrintToConsole(client, "!heavy");
				PrintToConsole(client, "!smg");
				PrintToConsole(client, "!smgs");
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Info Commands working for everybody:");
				PrintToConsole(client, "!info");
				PrintToConsole(client, "!adminfo");
				PrintToConsole(client, "!adminformation");
				PrintToConsole(client, "!information");
				PrintToConsole(client, "!author");
				PrintToConsole(client, "!creator");
				PrintToConsole(client, "!plugininfo");
				PrintToConsole(client, "!plugininformation");
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Any ideas for future features please contact %s", Author);
				PrintToConsole(client, "Contact %s on %s", Author, Url);
				PrintToConsole(client, "---------------------------------------------------------------------------");
			}
			case 5:
			{
			    GivePlayerItem(client, "weapon_usp_silencer");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EUSP-S\x01.");
			}
			case 6:
			{
			    GivePlayerItem(client, "weapon_glock");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EGlock-18\x01.");
			}
			case 7:
			{
			    GivePlayerItem(client, "weapon_elite");
				PrintToChat(client, "\x04[SM]\x01 You have given your self some \x0EDual Berretas\x01.");
			}
			case 8:
			{
			    GivePlayerItem(client, "weapon_p250");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EP250\x01.");
			}
			case 9:
			{
			    GivePlayerItem(client, "weapon_fiveseven");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EFive-SeveN\x01.");
			}
			case 10:
			{
			    GivePlayerItem(client, "weapon_tec9");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ETec-9");
			}
			case 11:
			{
			    GivePlayerItem(client, "weapon_cz75a");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ECZ75-Auto");
			}
			case 12:
			{
			    GivePlayerItem(client, "weapon_deagle");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EDesert Eagle\x01.");
			}
			case 13:
			{
			    GivePlayerItem(client, "weapon_galilar");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EGalil AR\x01.");
			}
			case 14:
			{
			    GivePlayerItem(client, "weapon_famas");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EFAMAS\x01.");
			}
			case 15:
			{
			    GivePlayerItem(client, "weapon_ak47");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EAK-47\x01.");
			}
			case 16:
			{
			    GivePlayerItem(client, "weapon_m4a1_silencer");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EM4A4\x01.");
			}
			case 17:
			{
			    GivePlayerItem(client, "weapon_m4a1");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EM4A1-S\x01.");
			}
			case 18:
			{
			    GivePlayerItem(client, "weapon_ssg08");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ESSG 08\x01.");
			}
			case 19:
			{
			    GivePlayerItem(client, "weapon_sg556");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ESG 553\x01.");
			}
			case 20:
			{
			    GivePlayerItem(client, "weapon_aug");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EAUG\x01.");
			}
			case 21:
			{
			    GivePlayerItem(client, "weapon_awp");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EAWP\x01.");
			}
			case 22:
			{
			    GivePlayerItem(client, "weapon_g3sg1");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EG3SG1\x01.");
			}
			case 23:
			{
			    GivePlayerItem(client, "weapon_scar20");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ESCAR-20\x01.");
			}
			case 24:
			{
			    GivePlayerItem(client, "weapon_nova");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ENova\x01.");
			}
			case 25:
			{
			    GivePlayerItem(client, "weapon_xm1014");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EXM1014\x01.");
			}
			case 26:
			{
			    GivePlayerItem(client, "weapon_mag7");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EMAG-7\x01.");
			}
			case 27:
			{
			    GivePlayerItem(client, "weapon_sawedoff");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ESawed-Off\x01.");
			}
			case 28:
			{
			    GivePlayerItem(client, "weapon_m249");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EM249\x01.");
			}
			case 29:
			{
			    GivePlayerItem(client, "weapon_negev");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ENegev\x01.");
			}
			case 30:
			{
			    GivePlayerItem(client, "weapon_mac10");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EMAC-10");
			}
			case 31:
			{
			    GivePlayerItem(client, "weapon_mp9");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EMP9\x01.");
			}
			case 32:
			{
			    GivePlayerItem(client, "weapon_mp7");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EMP7\x01.");
			}
			case 33:
			{
			    GivePlayerItem(client, "weapon_ump45");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EUMP-45\x01.");
			}
			case 34:
			{
			    GivePlayerItem(client, "weapon_p90");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EP90\x01.");
			}
			case 35:
			{
			    GivePlayerItem(client, "weapon_bizon");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EPP-Bizon\x01.");
			}
		}
	}
	else if (action == MenuAction_End )
	{
		CloseHandle(menuhandle1);
	}
}

public Action Cmd_SecondaryWeapons(int client, int args)
{
    new Handle:menuhandle2 = CreateMenu(Cmd_SecondaryWeapons2);
	SetMenuTitle(menuhandle2, "Admin Commands & Tools (All Secondary Weapons) Version: %s.\nPlugin Created By %s.", DATA, Author);
	
	AddMenuItem(menuhandle2, "usp_silencer", "USP-S");
	AddMenuItem(menuhandle2, "glock", "Glock-18");
	AddMenuItem(menuhandle2, "elite", "Dual Berretas");
	AddMenuItem(menuhandle2, "p250", "P250");
	AddMenuItem(menuhandle2, "fiveseven", "Five SeveN");
	AddMenuItem(menuhandle2, "tec9", "Tec-9")
	AddMenuItem(menuhandle2, "cz75a", "CZ75-Auto");
	AddMenuItem(menuhandle2, "deagle", "Desert Eagle");
	
	SetMenuPagination(menuhandle2, 7);
	SetMenuExitButton(menuhandle2, true);
	DisplayMenu(menuhandle2, client, 0);
	
	return Plugin_Handled;
}

public Cmd_SecondaryWeapons2(Handle:menuhandle2, MenuAction:action, client, ItemNum)
{
    if(action == MenuAction_Select )
	{
	    switch(ItemNum)
		{
		    case 0:
			{
			    GivePlayerItem(client, "weapon_usp_silencer");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EUSP-S\x01.");
			}
			case 1:
			{
			    GivePlayerItem(client, "weapon_glock");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EGlock-18\x01.");
			}
			case 2:
			{
			    GivePlayerItem(client, "weapon_elite");
				PrintToChat(client, "\x04[SM]\x01 You have given your self some \x0EDual Berretas\x01.");
			}
			case 3:
			{
			    GivePlayerItem(client, "weapon_p250");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EP250\x01.");
			}
			case 4:
			{
			    GivePlayerItem(client, "weapon_fiveseven");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EFive-SeveN\x01.");
			}
			case 5:
			{
			    GivePlayerItem(client, "weapon_tec9");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ETec-9");
			}
			case 6:
			{
			    GivePlayerItem(client, "weapon_cz75a");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ECZ75-Auto");
			}
			case 7:
			{
			    GivePlayerItem(client, "weapon_deagle");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EDesert Eagle\x01.");
			}
		}
	}
	else if (action == MenuAction_End )
	{
		CloseHandle(menuhandle2);
	}
}

public Action Cmd_PrimaryWeapons(int client, int args)
{
   new Handle:menuhandle3 = CreateMenu(Cmd_PrimaryWeapons2);
	SetMenuTitle(menuhandle3, "Admin Commands & Tools (All Primary Weapons) Version: %s.\nPlugin Created By %s.", DATA, Author);
	
	AddMenuItem(menuhandle3, "galilar", "Galil AR");
	AddMenuItem(menuhandle3, "famas", "FAMAS");
	AddMenuItem(menuhandle3, "ak47", "AK-47");
	AddMenuItem(menuhandle3, "m4a1_silencer", "M4A1-S");
	AddMenuItem(menuhandle3, "m4a1", "M4A4");
	AddMenuItem(menuhandle3, "ssg08", "SSG 08");
	AddMenuItem(menuhandle3, "sg553", "SG 553");
	AddMenuItem(menuhandle3, "aug", "AUG");
	AddMenuItem(menuhandle3, "awp", "AWP");
	AddMenuItem(menuhandle3, "g3sg1", "G3SG1");
	AddMenuItem(menuhandle3, "scar20", "SCAR-20");
	
	SetMenuPagination(menuhandle3, 7);
	SetMenuExitButton(menuhandle3, true);
	DisplayMenu(menuhandle3, client, 0);
	
	return Plugin_Handled;
}

public Cmd_PrimaryWeapons2(Handle:menuhandle3, MenuAction:action, client, ItemNum)
{
    if(action == MenuAction_Select )
	{
	    switch(ItemNum)
		{
		    case 0:
			{
			    GivePlayerItem(client, "weapon_galilar");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EGalil AR\x01.");
			}
			case 1:
			{
			    GivePlayerItem(client, "weapon_famas");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EFAMAS\x01.");
			}
			case 2:
			{
			    GivePlayerItem(client, "weapon_ak47");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EAK-47\x01.");
			}
			case 3:
			{
			    GivePlayerItem(client, "weapon_m4a1_silencer");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EM4A4\x01.");
			}
			case 4:
			{
			    GivePlayerItem(client, "weapon_m4a1");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EM4A1-S\x01.");
			}
			case 5:
			{
			    GivePlayerItem(client, "weapon_ssg08");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ESSG 08\x01.");
			}
			case 6:
			{
			    GivePlayerItem(client, "weapon_sg553");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ESG 553\x01.");
			}
			case 7:
			{
			    GivePlayerItem(client, "weapon_aug");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EAUG\x01.");
			}
			case 8:
			{
			    GivePlayerItem(client, "weapon_awp");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EAWP\x01.");
			}
			case 9:
			{
			    GivePlayerItem(client, "weapon_g3sg1");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EG3SG1\x01.");
			}
			case 10:
			{
			    GivePlayerItem(client, "weapon_scar20");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ESCAR-20\x01.");
			}
		}
	}
	else if (action == MenuAction_End )
	{
		CloseHandle(menuhandle3);
	}
}

public Action Cmd_HeavyWeapons(int client, int args)
{
    new Handle:menuhandle4 = CreateMenu(Cmd_HeavyWeapons2);
	SetMenuTitle(menuhandle4, "Admin Commands & Tools (All Heavy Weapons) Version: %s.\nPlugin Created By %s.", DATA, Author);
	
	AddMenuItem(menuhandle4, "nova", "Nova");
	AddMenuItem(menuhandle4, "xm1014", "XM1014");
	AddMenuItem(menuhandle4, "mag7", "MAG-7");
	AddMenuItem(menuhandle4, "sawedoff", "Sawed-Off");
	AddMenuItem(menuhandle4, "m249", "M249");
	AddMenuItem(menuhandle4, "negev", "Negev");
	
	SetMenuPagination(menuhandle4, 7);
	SetMenuExitButton(menuhandle4, true);
	DisplayMenu(menuhandle4, client, 0);
	
	return Plugin_Handled;
}

public Cmd_HeavyWeapons2(Handle:menuhandle4, MenuAction:action, client, ItemNum)
{
    if(action == MenuAction_Select )
	{
	    switch(ItemNum)
		{
		    case 0:
			{
			    GivePlayerItem(client, "weapon_nova");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ENova\x01.");
			}
			case 1:
			{
			    GivePlayerItem(client, "weapon_xm1014");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EXM1014\x01.");
			}
			case 2:
			{
			    GivePlayerItem(client, "weapon_mag7");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EMAG-7\x01.");
			}
			case 3:
			{
			    GivePlayerItem(client, "weapon_sawedoff");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ESawed-Off\x01.");
			}
			case 4:
			{
			    GivePlayerItem(client, "weapon_m249");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EM249\x01.");
			}
			case 5:
			{
			    GivePlayerItem(client, "weapon_negev");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0ENegev\x01.");
			}
		}
	}
	else if (action == MenuAction_End )
	{
		CloseHandle(menuhandle4);
	}
}

public Action Cmd_SMGWeapons(int client, int args)
{
    new Handle:menuhandle5 = CreateMenu(Cmd_SMGWeapons2);
	SetMenuTitle(menuhandle5, "Admin Commands & Tools (All SMGs Weapons) Version: %s.\nPlugin Created By %s.", DATA, Author);
	
	AddMenuItem(menuhandle5, "mac10", "MAC-10");
	AddMenuItem(menuhandle5, "mp9", "MP9");
	AddMenuItem(menuhandle5, "mp7", "MP7");
	AddMenuItem(menuhandle5, "ump45", "UMP-45");
	AddMenuItem(menuhandle5, "p90", "P90");
	AddMenuItem(menuhandle5, "bizon", "PP-Bizon");
	
	SetMenuPagination(menuhandle5, 7);
	SetMenuExitButton(menuhandle5, true);
	DisplayMenu(menuhandle5, client, 0);
	
	return Plugin_Handled;
}

public Cmd_SMGWeapons2(Handle:menuhandle5, MenuAction:action, client, ItemNum)
{
    if(action == MenuAction_Select )
	{
	    switch(ItemNum)
		{
		    case 0:
			{
			    GivePlayerItem(client, "weapon_mac10");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EMAC-10");
			}
			case 1:
			{
			    GivePlayerItem(client, "weapon_mp9");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EMP9\x01.");
			}
			case 2:
			{
			    GivePlayerItem(client, "weapon_mp7");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EMP7\x01.");
			}
			case 3:
			{
			    GivePlayerItem(client, "weapon_ump45");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EUMP-45\x01.");
			}
			case 4:
			{
			    GivePlayerItem(client, "weapon_p90");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EP90\x01.");
			}
			case 5:
			{
			    GivePlayerItem(client, "weapon_bizon");
				PrintToChat(client, "\x04[SM]\x01 You have given your self a \x0EPP-Bizon\x01.");
			}
		}
	}
	else if (action == MenuAction_End )
	{
		CloseHandle(menuhandle5);
	}
}

public Action Cmd_AdminCommandsInfo(int client, int args)
{
    new Handle:menuhandle6 = CreateMenu(Cmd_AdminCommandsInfo2);
	SetMenuTitle(menuhandle6, "Admin Commands & Tools (Info) Version: %s.\nPlugin Created By %s.", DATA, Author);
	
	AddMenuItem(menuhandle6, "plugininfo", "This plugin is for admins give them selvs some weapons with !admintools and other commands.", ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle6, "plugininfo2", "It is possible to for admins to use some game commands without they need a rcon password", ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle6, "plugininfo3", "More features will come.");
	
	SetMenuPagination(menuhandle6, 7);
	SetMenuExitButton(menuhandle6, true);
	DisplayMenu(menuhandle6, client, 0);
	
	return Plugin_Handled;
}

public Cmd_AdminCommandsInfo2(Handle:menuhandle6, MenuAction:action, client, ItemNum)
{
    if(action == MenuAction_Select )
	{
	    switch(ItemNum)
		{
		    case 0:
			{
			    PrintToChat(client, "\x04[SM]\x01 The Creator of this plugin is \x0E%s\x01. More Plugin Info Check Console.", Author);
				PrintToConsole(client, "---------------------------------------------------------------------------");
				PrintToConsole(client, "Author = %s", Author);
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Commands for this plugin working for all admins:");
				PrintToConsole(client, "!admintools");
				PrintToConsole(client, "!admintool");
				PrintToConsole(client, "!admincommands");
				PrintToConsole(client, "!admincommand");
				PrintToConsole(client, "!admincmds");
				PrintToConsole(client, "!admincmd");
				PrintToConsole(client, "!restartgame");
				PrintToConsole(client, "!restart");
				PrintToConsole(client, "!pausematch");
				PrintToConsole(client, "!pause");
				PrintToConsole(client, "!unpausematch");
				PrintToConsole(client, "!unpause");
				PrintToConsole(client, "!secondaryweapons");
				PrintToConsole(client, "!secondaryweapon");
				PrintToConsole(client, "!secondary");
				PrintToConsole(client, "!secondarys");
				PrintToConsole(client, "!pistols");
				PrintToConsole(client, "!pistol");
				PrintToConsole(client, "!primaryweapons");
				PrintToConsole(client, "!primaryweapon");
				PrintToConsole(client, "!primary");
				PrintToConsole(client, "!primarys");
				PrintToConsole(client, "!rifles");
				PrintToConsole(client, "!rifle");
				PrintToConsole(client, "!heavyweapons");
				PrintToConsole(client, "!heavy");
				PrintToConsole(client, "!smg");
				PrintToConsole(client, "!smgs");
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Info Commands working for everybody:");
				PrintToConsole(client, "!info");
				PrintToConsole(client, "!adminfo");
				PrintToConsole(client, "!adminformation");
				PrintToConsole(client, "!information");
				PrintToConsole(client, "!author");
				PrintToConsole(client, "!creator");
				PrintToConsole(client, "!plugininfo");
				PrintToConsole(client, "!plugininformation");
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Any ideas for future features please contact %s", Author);
				PrintToConsole(client, "Contact %s on %s", Author, Url);
				PrintToConsole(client, "---------------------------------------------------------------------------");
			}
			case 1:
			{
			    PrintToChat(client, "\x04[SM]\x01 The Creator of this plugin is \x0E%s\x01. More Plugin Info Check Console.", Author);
				PrintToConsole(client, "---------------------------------------------------------------------------");
				PrintToConsole(client, "Author = %s", Author);
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Commands for this plugin working for all admins:");
				PrintToConsole(client, "!admintools");
				PrintToConsole(client, "!admintool");
				PrintToConsole(client, "!admincommands");
				PrintToConsole(client, "!admincommand");
				PrintToConsole(client, "!admincmds");
				PrintToConsole(client, "!admincmd");
				PrintToConsole(client, "!restartgame");
				PrintToConsole(client, "!restart");
				PrintToConsole(client, "!pausematch");
				PrintToConsole(client, "!pause");
				PrintToConsole(client, "!unpausematch");
				PrintToConsole(client, "!unpause");
				PrintToConsole(client, "!secondaryweapons");
				PrintToConsole(client, "!secondaryweapon");
				PrintToConsole(client, "!secondary");
				PrintToConsole(client, "!secondarys");
				PrintToConsole(client, "!pistols");
				PrintToConsole(client, "!pistol");
				PrintToConsole(client, "!primaryweapons");
				PrintToConsole(client, "!primaryweapon");
				PrintToConsole(client, "!primary");
				PrintToConsole(client, "!primarys");
				PrintToConsole(client, "!rifles");
				PrintToConsole(client, "!rifle");
				PrintToConsole(client, "!heavyweapons");
				PrintToConsole(client, "!heavy");
				PrintToConsole(client, "!smg");
				PrintToConsole(client, "!smgs");
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Info Commands working for everybody:");
				PrintToConsole(client, "!info");
				PrintToConsole(client, "!adminfo");
				PrintToConsole(client, "!adminformation");
				PrintToConsole(client, "!information");
				PrintToConsole(client, "!author");
				PrintToConsole(client, "!creator");
				PrintToConsole(client, "!plugininfo");
				PrintToConsole(client, "!plugininformation");
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Any ideas for future features please contact %s", Author);
				PrintToConsole(client, "Contact %s on %s", Author, Url);
				PrintToConsole(client, "---------------------------------------------------------------------------");
			}
			case 2:
			{
			    PrintToChat(client, "\x04[SM]\x01 The Creator of this plugin is \x0E%s\x01. More Plugin Info Check Console.", Author);
				PrintToConsole(client, "---------------------------------------------------------------------------");
				PrintToConsole(client, "Author = %s", Author);
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Commands for this plugin working for all admins:");
				PrintToConsole(client, "!admintools");
				PrintToConsole(client, "!admintool");
				PrintToConsole(client, "!admincommands");
				PrintToConsole(client, "!admincommand");
				PrintToConsole(client, "!admincmds");
				PrintToConsole(client, "!admincmd");
				PrintToConsole(client, "!restartgame");
				PrintToConsole(client, "!restart");
				PrintToConsole(client, "!pausematch");
				PrintToConsole(client, "!pause");
				PrintToConsole(client, "!unpausematch");
				PrintToConsole(client, "!unpause");
				PrintToConsole(client, "!secondaryweapons");
				PrintToConsole(client, "!secondaryweapon");
				PrintToConsole(client, "!secondary");
				PrintToConsole(client, "!secondarys");
				PrintToConsole(client, "!pistols");
				PrintToConsole(client, "!pistol");
				PrintToConsole(client, "!primaryweapons");
				PrintToConsole(client, "!primaryweapon");
				PrintToConsole(client, "!primary");
				PrintToConsole(client, "!primarys");
				PrintToConsole(client, "!rifles");
				PrintToConsole(client, "!rifle");
				PrintToConsole(client, "!heavyweapons");
				PrintToConsole(client, "!heavy");
				PrintToConsole(client, "!smg");
				PrintToConsole(client, "!smgs");
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Info Commands working for everybody:");
				PrintToConsole(client, "!info");
				PrintToConsole(client, "!adminfo");
				PrintToConsole(client, "!adminformation");
				PrintToConsole(client, "!information");
				PrintToConsole(client, "!author");
				PrintToConsole(client, "!creator");
				PrintToConsole(client, "!plugininfo");
				PrintToConsole(client, "!plugininformation");
				PrintToConsole(client, "        ");
				PrintToConsole(client, "Any ideas for future features please contact %s", Author);
				PrintToConsole(client, "Contact %s on %s", Author, Url);
				PrintToConsole(client, "---------------------------------------------------------------------------");
			}
		}
	}
	else if (action == MenuAction_End )
	{
		CloseHandle(menuhandle6);
	}
}

public Action Cmd_Author(client, args)
{
    PrintHintText(client, "The Creator of this plugin is \x0E%s\x01. Check Console for more info.", Author);
    PrintToChat(client, "\x04[SM]\x01 The Creator of this plugin is \x0E%s\x01. Check Console for more info.", Author);
	PrintToConsole(client, "---------------------------------------------------------------------------");
	PrintToConsole(client, "Name = %s", Author);
	PrintToConsole(client, "Real Name = Markus");
	PrintToConsole(client, "Steam Url = %s", Url);
	PrintToConsole(client, "Currently Community = CSROG");
	PrintToConsole(client, "Community Steam Group = http://steamcommunity.com/groups/csrogcommunity");
	PrintToConsole(client, "Community Forum = http://csrog.com");
    PrintToConsole(client, "%s Is also making private plugins for a small bit of money.", Author);
	PrintToConsole(client, "---------------------------------------------------------------------------");
}

public Action Cmd_PluginInfo(client, args)
{
    PrintHintText(client, " The Creator of this plugin is \x0E%s\x01. More Plugin Info Check Console.", Author);
	PrintToChat(client, "\x04[SM]\x01 The Creator of this plugin is \x0E%s\x01. More Plugin Info Check Console.", Author);
	PrintToConsole(client, "---------------------------------------------------------------------------");
	PrintToConsole(client, "Author = %s", Author);
	PrintToConsole(client, "        ");
	PrintToConsole(client, "Commands for this plugin working for all admins:");
	PrintToConsole(client, "!admintools");
	PrintToConsole(client, "!admintool");
	PrintToConsole(client, "!admincommands");
	PrintToConsole(client, "!admincommand");
	PrintToConsole(client, "!admincmds");
    PrintToConsole(client, "!admincmd");
	PrintToConsole(client, "!restartgame");
	PrintToConsole(client, "!restart");
	PrintToConsole(client, "!pausematch");
	PrintToConsole(client, "!pause");
	PrintToConsole(client, "!unpausematch");
	PrintToConsole(client, "!unpause");
	PrintToConsole(client, "!secondaryweapons");
	PrintToConsole(client, "!secondaryweapon");
	PrintToConsole(client, "!secondary");
	PrintToConsole(client, "!secondarys");
	PrintToConsole(client, "!pistols");
	PrintToConsole(client, "!pistol");
	PrintToConsole(client, "!primaryweapons");
	PrintToConsole(client, "!primaryweapon");
	PrintToConsole(client, "!primary");
	PrintToConsole(client, "!primarys");
	PrintToConsole(client, "!rifles");
	PrintToConsole(client, "!rifle");
	PrintToConsole(client, "!heavyweapons");
	PrintToConsole(client, "!heavy");
	PrintToConsole(client, "!smg");
	PrintToConsole(client, "!smgs");
	PrintToConsole(client, "        ");
	PrintToConsole(client, "Info Commands working for everybody:");
	PrintToConsole(client, "!info");
	PrintToConsole(client, "!adminfo");
	PrintToConsole(client, "!adminformation");
	PrintToConsole(client, "!information");
	PrintToConsole(client, "!author");
	PrintToConsole(client, "!creator");
	PrintToConsole(client, "!plugininfo");
	PrintToConsole(client, "!plugininformation");
	PrintToConsole(client, "        ");
	PrintToConsole(client, "Any ideas for future features please contact %s", Author);
	PrintToConsole(client, "Contact %s on %s", Author, Url);
	PrintToConsole(client, "---------------------------------------------------------------------------");
}

public Action Cmd_RestartGame(client, args)
{
    PrintHintText(client, "Restarting Match!");
    ServerCommand("mp_restartgame 5");
	PrintToChat(client, "\x04[SM]\x01 Restarting Match!");
	PrintToChat(client, "\x04[SM]\x01 Restarting Match!");
	PrintToChat(client, "\x04[SM]\x01 Restarting Match!");
}

public Action Cmd_PauseMatch(client, args)
{
    PrintHintText(client, "Pausing Match");
	ServerCommand("mp_pause_match 1");
	PrintToChat(client, "\x04[SM]\x01 Pausing Match!");
	PrintToChat(client, "\x04[SM]\x01 Pausing Match!");
	PrintToChat(client, "\x04[SM]\x01 Pausing Match!");
}

public Action Cmd_UnPauseMatch(client, args)
{
    PrintHintText(client, "Unpausing Match!");
	ServerCommand("mp_unpause_match 1");
	PrintToChat(client, "\x04[SM]\x01 Unpausing Match!");
	PrintToChat(client, "\x04[SM]\x01 Unpausing Match!");
	PrintToChat(client, "\x04[SM]\x01 Unpausing Match!");
}